package talent;

public class Telefono {
	private String numero;
	private String operador;
	private String marca;

	public Telefono(String _numero, String _operador, String _marca) {
		this.numero = _numero;
		this.operador = _operador;
		this.marca = _marca;
	}
	
	public Telefono(String _operador, String _marca) {
		this.operador = _operador;
		this.marca = _marca;
		this.numero = String.valueOf((Math.random()* 799999999) + 600000000);
	}
	
	public void imprimeNumero() {
		System.out.println("El n�mero es: " + this.numero);
	}

	public void imprimeOperador() {
		System.out.println("El operador es: " + this.operador);
	}

	public void imprimeMarca() {
		System.out.println("La marca es: " + this.marca);
	}
}
