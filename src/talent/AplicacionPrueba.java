package talent;

import java.util.Scanner;

public class AplicacionPrueba {

	public static void main(String[] args) {
		AplicacionPrueba.ejercicio1();
		AplicacionPrueba.ejercicio2();
		AplicacionPrueba.ejercicio3();
		AplicacionPrueba.ejercicio4();
		AplicacionPrueba.ejercicio5(5);
	}

	public static void ejercicio1() {
		Persona arturoPersona = new Persona("Arturo", "Pardo", 33);
		arturoPersona.crearTelefono("=Orange", "IPhone");
		AplicacionPrueba.imprimirDatosPersona(arturoPersona);
	}

	public static void ejercicio2() {
		Persona persona1;
		Persona persona2;
		Persona persona3;

		persona1 = AplicacionPrueba.pedirDatosCrearPersona();
		persona2 = AplicacionPrueba.pedirDatosCrearPersona();
		persona3 = AplicacionPrueba.pedirDatosCrearPersona();

		AplicacionPrueba.imprimirDatosPersona(persona1);
		AplicacionPrueba.imprimirDatosPersona(persona2);
		AplicacionPrueba.imprimirDatosPersona(persona3);
	}

	public static void ejercicio3() {
		String numero;
		String operador;
		String marca;
		Scanner scanner = new Scanner(System.in);
		Persona yoMismo = new Persona("Vicente", "Quiles", 33);

		System.out.println("Introduce n�mero:");
		numero = scanner.nextLine();
		System.out.println("Introduce operador:");
		operador = scanner.nextLine();
		System.out.println("Introduce marca:");
		marca = scanner.nextLine();

		yoMismo.crearTelefono(numero, operador, marca);
	}

	public static void ejercicio4() {
		Persona yoMismo = new Persona("Vicente", "Quiles", 33);
		yoMismo.imprimeDatosTelefono();
		yoMismo.crearTelefono("Vodafone", "LG");
	}

	public static void ejercicio5(int cantidadPersonas) {
		Persona[] listadoPersonas = new Persona[cantidadPersonas];

		for (int i = 0; i < cantidadPersonas; i++) {
			listadoPersonas[i] = AplicacionPrueba.pedirDatosCrearPersona();
		}
		
		for (int i = 0; i < cantidadPersonas; i++) {
			AplicacionPrueba.imprimirDatosPersona(listadoPersonas[i]);
		}
	}

	private static void imprimirDatosPersona(Persona persona) {
		persona.imprimeNombre();
		persona.imprimeApellido();
		persona.imprimeEdad();
		persona.imprimeDatosTelefono();
	}

	private static Persona pedirDatosCrearPersona() {
		Persona persona;
		String nombre;
		String apellido;
		int edad;
		Scanner scanner = new Scanner(System.in);

		System.out.println("Introduce tu nombre:");
		nombre = scanner.nextLine();
		System.out.println("Introduce tu apellido:");
		apellido = scanner.nextLine();
		System.out.println("Introduce tu edad:");
		edad = Integer.parseInt(scanner.nextLine());
		System.out.println("Introduce tu operador:");
		String operador = scanner.nextLine();
		System.out.println("Introduce tu marca:");
		String marca = scanner.nextLine();

		persona = new Persona(nombre, apellido, edad);
		persona.crearTelefono(operador, marca);

		return persona;
	}

}
