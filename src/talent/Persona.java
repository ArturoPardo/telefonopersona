package talent;

public class Persona {
	private String nombre;
	private String apellido;
	private int edad;
	private Telefono telefono;

	public Persona(String _nombre, String _apellido, int _edad) {
		this.nombre = _nombre;
		this.apellido = _apellido;
		this.edad = _edad;
	}

	public void crearTelefono(String numero, String operador, String marca) {
		this.telefono = new Telefono(numero, operador, marca);
	}

	public void crearTelefono(String operador, String marca) {
		this.telefono = new Telefono(operador, marca);
	}

	public void imprimeNombre() {
		System.out.println("Mi nombre es " + this.nombre);
	}

	public void imprimeApellido() {
		System.out.println("Mi apellido es " + this.apellido);
	}

	public void imprimeEdad() {
		System.out.println("Tengo " + this.edad + " a�os.");
	}

	public void imprimeDatosTelefono() {
		if (this.telefono != null) {
			this.telefono.imprimeMarca();
			this.telefono.imprimeOperador();
			this.telefono.imprimeNumero();
		}
	}
}
